<?php


namespace Plusforta\Assert;

use Plusforta\Assert\Exception\InvalidArgumentException;
use Symfony\Component\Validator\Constraints\Iban;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validation;

final class Assert
{

    public static function iban(string $iban): void
    {
        $validator = Validation::createValidator();
        $violations = $validator->validate($iban, new Iban());

        if (count($violations) > 0) {
            /** @var ConstraintViolation $violation */
            $violation = $violations[0];
            throw new InvalidArgumentException(sprintf('%s Got: "%s"', $violation->getMessage(), $iban));
        }
    }

}