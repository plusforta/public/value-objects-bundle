<?php


namespace Plusforta\ValueObjects;


interface StringValueInterface
{

    public static function fromString(string $value): self;

    public function toString(): string;

}