<?php


namespace Plusforta\ValueObjects\Common\Payment;


use Plusforta\ValueObjects\StringValueInterface;
use Symfony\Component\Serializer\Exception\CircularReferenceException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\LogicException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Webmozart\Assert\Assert;

final class PaymentFrequency
{
    public const PAYMENT_MONTHLY = 'monthly';
    public const PAYMENT_QUARTERLY = 'quarterly';
    public const PAYMENT_SEMI_ANNUALLY = 'semi-annually';
    public const PAYMENT_ANNUALLY = 'annually';

    private const ALLOWED_PAYMENTS = [
        self::PAYMENT_MONTHLY,
        self::PAYMENT_QUARTERLY,
        self::PAYMENT_SEMI_ANNUALLY,
        self::PAYMENT_ANNUALLY
    ];

    private string $paymentFrequency;

    private function __construct(string $paymentFrequency)
    {
        $this->paymentFrequency = $paymentFrequency;
    }


    public static function fromString(string $paymentFrequency): self
    {
        Assert::oneOf($paymentFrequency, self::ALLOWED_PAYMENTS);
        return new self($paymentFrequency);
    }

    public function toString(): string
    {
        return $this->paymentFrequency;
    }

    public static function monthly(): self
    {
        return self::fromString(self::PAYMENT_MONTHLY);
    }

    public static function quarterly(): self
    {
        return self::fromString(self::PAYMENT_QUARTERLY);
    }

    public static function semiAnnually(): self
    {
        return self::fromString(self::PAYMENT_SEMI_ANNUALLY);
    }

    public static function annually(): self
    {
        return self::fromString(self::PAYMENT_ANNUALLY);
    }

    public function __toString(): string
    {
        return $this->paymentFrequency;
    }

    public function isMonthly(): bool
    {
        return $this->paymentFrequency === self::PAYMENT_MONTHLY;
    }

    public function isQuarterly(): bool
    {
        return $this->paymentFrequency === self::PAYMENT_QUARTERLY;
    }

    public function isSemiAnnually(): bool
    {
        return $this->paymentFrequency === self::PAYMENT_SEMI_ANNUALLY;
    }

    public function isAnnually(): bool
    {
        return $this->paymentFrequency === self::PAYMENT_ANNUALLY;
    }

}