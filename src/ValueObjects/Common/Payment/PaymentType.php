<?php


namespace Plusforta\ValueObjects\Common\Payment;


use Plusforta\ValueObjects\StringValueInterface;
use Webmozart\Assert\Assert;

final class PaymentType implements StringValueInterface
{
    public const TYPE_SEPA = 'sepa';

    private const ALLOWED_TYPES = [
        self::TYPE_SEPA
    ];

    private string $type;

    private function __construct(string $type)
    {
        $this->type = $type;
    }

    public static function fromString(string $iban): self
    {
        Assert::oneOf($iban, self::ALLOWED_TYPES);
        return new self($iban);
    }

    public function toString(): string
    {
        return $this->type;
    }

    public static function sepa(): self
    {
        return self::fromString(self::TYPE_SEPA);
    }

    public function isSepa(): bool
    {
        return $this->type === self::TYPE_SEPA;
    }
}