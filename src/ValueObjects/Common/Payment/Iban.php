<?php


namespace Plusforta\ValueObjects\Common\Payment;


use Plusforta\Assert\Assert;
use Plusforta\ValueObjects\StringValueInterface;

final class Iban implements StringValueInterface
{
    private string $iban;

    private function __construct(string $iban)
    {
        $this->iban = $iban;
    }


    public static function fromString(string $iban): self
    {
        Assert::iban($iban);
        return new self($iban);
    }

    public static function normalizedFromString(string $iban): self
    {
        $iban = preg_replace('/\s+/', '', $iban);
        return self::fromString(strtoupper($iban));
    }

    public function toString(): string
    {
        return $this->iban;
    }
}