<?php


namespace Plusforta\ValueObjects\Common\Tenant;


use Plusforta\ValueObjects\StringValueInterface;
use Webmozart\Assert\Assert;

final class Gender implements StringValueInterface
{
    public const GENDER_FEMALE = 'female';
    public const GENDER_MALE = 'male';

    private const ALLOWED_GENDER = [
        self::GENDER_FEMALE,
        self::GENDER_MALE,
    ];

    private string $gender;

    private function __construct(string $gender)
    {
        $this->gender = $gender;
    }


    public static function fromString(string $gender): self
    {
        Assert::oneOf($gender, self::ALLOWED_GENDER);
        return new self($gender);
    }

    public function toString(): string
    {
        return $this->gender;
    }

    public static function female(): self
    {
        return self::fromString(self::GENDER_FEMALE);
    }

    public static function male(): self
    {
        return self::fromString(self::GENDER_MALE);
    }

    public function isFemale(): bool
    {
        return $this->gender === self::GENDER_FEMALE;
    }

    public function isMale(): bool
    {
        return $this->gender === self::GENDER_MALE;
    }

    public function equals(?Gender $gender): bool
    {
        if ($gender === null) {
            return false;
        }

        return ($this->isFemale() && $gender->isFemale())
            || ($this->isMale() && $gender->isMale());
    }



}