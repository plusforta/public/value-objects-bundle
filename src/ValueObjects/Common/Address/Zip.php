<?php


namespace Plusforta\ValueObjects\Common\Address;


use Plusforta\ValueObjects\StringValueInterface;
use Webmozart\Assert\Assert;

final class Zip implements StringValueInterface
{
    private string $zip;

    private function __construct(string $zip)
    {
        $this->zip = $zip;
    }


    public static function fromString(string $zip): self
    {
        Assert::length($zip, 5);
        Assert::digits($zip);

        return new self($zip);
    }

    public function toString(): string
    {
        return $this->zip;
    }

    public function equals(?Zip $zip): bool
    {
        if ($zip === null) {
            return false;
        }

        return $this->zip === $zip->toString();
    }
}