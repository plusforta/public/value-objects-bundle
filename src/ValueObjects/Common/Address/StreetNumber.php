<?php


namespace Plusforta\ValueObjects\Common\Address;


use Plusforta\ValueObjects\StringValueInterface;
use Webmozart\Assert\Assert;

final class StreetNumber implements StringValueInterface
{
    private string $streetNumber;

    private function __construct(string $streetNumber)
    {
        $this->streetNumber = $streetNumber;
    }


    public static function fromString(string $streetNumber): self
    {
        Assert::regex($streetNumber, '/.*[0-9].*/');

        return new self($streetNumber);
    }

    public function toString(): string
    {
        return $this->streetNumber;
    }

    public function equals(?StreetNumber $streetNumber): bool
    {
        if ($streetNumber === null) {
            return false;
        }

        return $this->streetNumber === $streetNumber->toString();
    }
}