<?php


namespace Plusforta\ValueObjects\Common\Export;


use Plusforta\ValueObjects\StringValueInterface;
use Webmozart\Assert\Assert;

final class ExportType implements StringValueInterface
{
    public const TYPE_RESERVATION = 'reservation';
    public const TYPE_APPLICATION = 'application';

    public const ALLOWED_TYPES = [
        self::TYPE_RESERVATION,
        self::TYPE_APPLICATION
    ];

    private string $type;

    private function __construct(string $type)
    {
        $this->type = $type;
    }

    public static function fromString(string $type): self
    {
        Assert::oneOf($type, self::ALLOWED_TYPES);
        return new self($type);
    }

    public function toString(): string
    {
        return $this->type;
    }

    public static function application(): self
    {
        return self::fromString(self::TYPE_APPLICATION);
    }

    public static function reservation(): self
    {
        return self::fromString(self::TYPE_RESERVATION);
    }


}