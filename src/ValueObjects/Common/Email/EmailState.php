<?php


namespace Plusforta\ValueObjects\Common\Email;


use Plusforta\ValueObjects\StringValueInterface;
use Webmozart\Assert\Assert;

class EmailState implements StringValueInterface
{
    public const STAUS_CREATED = 'created';
    public const STAUS_SUBMITTED = 'submitted';
    public const STAUS_DELIVERED = 'delivered';
    public const STAUS_BOUNCED = 'bounced';

    public const ALLOWED_STATES = [
        self::STAUS_CREATED,
        self::STAUS_SUBMITTED,
        self::STAUS_DELIVERED,
        self::STAUS_BOUNCED,
    ];

    private string $status;

    private function __construct(string $status)
    {
        $this->status = $status;
    }


    public static function fromString(string $status): self
    {
        Assert::oneOf($status, self::ALLOWED_STATES);
        return new self($status);
    }

    public function toString(): string
    {
        return $this->status;
    }

    public static function created(): self
    {
        return self::fromString(self::STAUS_CREATED);
    }

    public static function submitted(): self
    {
        return self::fromString(self::STAUS_SUBMITTED);
    }

    public static function delivered(): self
    {
        return self::fromString(self::STAUS_DELIVERED);
    }

    public static function bounced(): self
    {
        return self::fromString(self::STAUS_BOUNCED);
    }
}