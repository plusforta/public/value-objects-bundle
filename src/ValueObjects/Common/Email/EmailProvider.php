<?php


namespace Plusforta\ValueObjects\Common\Email;



use Plusforta\ValueObjects\StringValueInterface;
use Webmozart\Assert\Assert;

class EmailProvider implements StringValueInterface
{
    public const EMAIL_PROVIDER_POSTMARK = 'postmark';
    public const EMAIL_PROVIDER_MAILEON = 'maileon';
    public const EMAIL_PROVIDER_SMTP = 'smtp';

    public const ALLOWED_EMAIL_PROVIDER = [
        self::EMAIL_PROVIDER_POSTMARK,
        self::EMAIL_PROVIDER_MAILEON,
        self::EMAIL_PROVIDER_SMTP,
    ];

    private string $emailProvider;

    private function __construct(string $emailProvider)
    {

        $this->emailProvider = $emailProvider;
    }


    public static function fromString(string $storageProvider): self
    {
        Assert::oneOf($storageProvider, self::ALLOWED_EMAIL_PROVIDER);

        return new self($storageProvider);
    }

    public function toString(): string
    {
        return $this->emailProvider;
    }

    public static function postmark(): self
    {
        return self::fromString(self::EMAIL_PROVIDER_POSTMARK);
    }

    public static function maileon(): self
    {
        return self::fromString(self::EMAIL_PROVIDER_MAILEON);
    }

    public static function smtp(): self
    {
        return self::fromString(self::EMAIL_PROVIDER_SMTP);
    }

    public function isPostmark(): bool
    {
        return $this->emailProvider === self::EMAIL_PROVIDER_POSTMARK;
    }

    public function isMaileon(): bool
    {
        return $this->emailProvider === self::EMAIL_PROVIDER_MAILEON;
    }
}