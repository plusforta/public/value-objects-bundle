<?php


namespace Plusforta\ValueObjects\Common\Email;



use Plusforta\ValueObjects\StringValueInterface;
use Webmozart\Assert\Assert;

final class EmailType implements StringValueInterface
{
    public const EMAIL_TYPE_RESERVATION_ACCEPTED = 'reservation_accepted';
    public const EMAIL_TYPE_RESERVATION_DECLINED = 'reservation_declined';
    public const EMAIL_TYPE_RESERVATION_PENDING_REMINDER = 'reservation_pending';
    public const EMAIL_TYPE_RESERVATION_PENDING_SECOND_REMINDER = 'reservation_pending_followup';
    public const EMAIL_TYPE_APPLICATION_ACCEPTED = 'application_accepted';
    public const EMAIL_TYPE_APPLICATION_ACCEPTED_LANDLORD = 'application_accepted_landlord';
    public const EMAIL_TYPE_APPLICATION_DECLINED = 'application_declined';
    public const EMAIL_TYPE_APPLICATION_PENDING = 'application_pending';
    public const EMAIL_TYPE_SYSTEM_INSURANCE_TRANSFER = 'system_insurance_transfer';
    public const EMAIL_TYPE_DIGITAL_CERTIFICATE = 'digital_certificate';
    public const EMAIL_TYPE_EKOMI = 'ekomi';
    public const EMAIL_TYPE_SAVE_PROGRESS = 'save_progress';
    public const EMAIL_TYPE_UNKNOWN = 'unknown';
    public const TYPE_DELAYED_CERTIFICATE = 'delayed_certificate';
    public const TYPE_FAILED_DIGITAL_CERTIFICATE = 'failed_digital_certificate';


    public const ALLOWED_EMAIL_TYPES = [
        self::EMAIL_TYPE_RESERVATION_ACCEPTED,
        self::EMAIL_TYPE_RESERVATION_DECLINED,
        self::EMAIL_TYPE_RESERVATION_PENDING_REMINDER,
        self::EMAIL_TYPE_RESERVATION_PENDING_SECOND_REMINDER,
        self::EMAIL_TYPE_APPLICATION_ACCEPTED,
        self::EMAIL_TYPE_DIGITAL_CERTIFICATE,
        self::EMAIL_TYPE_APPLICATION_ACCEPTED_LANDLORD,
        self::EMAIL_TYPE_APPLICATION_DECLINED,
        self::EMAIL_TYPE_APPLICATION_PENDING,
        self::EMAIL_TYPE_SYSTEM_INSURANCE_TRANSFER,
        self::EMAIL_TYPE_EKOMI,
        self::EMAIL_TYPE_SAVE_PROGRESS,
        self::EMAIL_TYPE_UNKNOWN,
        self::TYPE_DELAYED_CERTIFICATE,
        self::TYPE_FAILED_DIGITAL_CERTIFICATE,
    ];

    private string $emailType;

    private function __construct(string $emailType)
    {
        $this->emailType = $emailType;
    }


    public static function fromString(string $storageType): self
    {
        Assert::oneOf($storageType, self::ALLOWED_EMAIL_TYPES);

        return new self($storageType);
    }

    public function toString(): string
    {
        return $this->emailType;
    }

    public static function reservationAccepted(): self
    {
        return self::fromString(self::EMAIL_TYPE_RESERVATION_ACCEPTED);
    }

    public static function reservationDeclined(): self
    {
        return self::fromString(self::EMAIL_TYPE_RESERVATION_DECLINED);
    }

    public static function reservationPendingReminder(): self
    {
        return self::fromString(self::EMAIL_TYPE_RESERVATION_PENDING_REMINDER);
    }

    public static function reservationPendingSecondReminder(): self
    {
        return self::fromString(self::EMAIL_TYPE_RESERVATION_PENDING_SECOND_REMINDER);
    }

    public static function applicationAccepted(): self
    {
        return self::fromString(self::EMAIL_TYPE_APPLICATION_ACCEPTED);
    }

    public static function applicationAcceptedLandlord(): self
    {
        return self::fromString(self::EMAIL_TYPE_APPLICATION_ACCEPTED_LANDLORD);
    }

    public static function applicationDeclined(): self
    {
        return self::fromString(self::EMAIL_TYPE_APPLICATION_DECLINED);
    }

    public static function applicationPending(): self
    {
        return self::fromString(self::EMAIL_TYPE_APPLICATION_PENDING);
    }

    public static function systemInsuranceTransfer(): self
    {
        return self::fromString(self::EMAIL_TYPE_SYSTEM_INSURANCE_TRANSFER);
    }

    public static function ekomi(): self
    {
        return self::fromString(self::EMAIL_TYPE_EKOMI);
    }

    public static function saveProgress(): self
    {
        return self::fromString(self::EMAIL_TYPE_SAVE_PROGRESS);
    }

    public static function unknown(): self
    {
        return self::fromString(self::EMAIL_TYPE_UNKNOWN);
    }

    public static function digitalCertificate(): self
    {
        return self::fromString(self::EMAIL_TYPE_DIGITAL_CERTIFICATE);
    }

    public static function delayedCertificate(): self
    {
        return self::fromString(self::TYPE_DELAYED_CERTIFICATE);
    }

    public static function failedDigitalCertificate(): self
    {
        return self::fromString(self::TYPE_FAILED_DIGITAL_CERTIFICATE);
    }
}
