<?php


namespace Plusforta\ValueObjects\Common\Email;



use Plusforta\ValueObjects\StringValueInterface;
use Webmozart\Assert\Assert;

final class Email implements StringValueInterface
{
    private string $email;

    private function __construct(string $email)
    {
        $this->email = $email;
    }

    public static function fromString(string $storage): self
    {
        Assert::email($storage);
        return new self($storage);
    }

    public function toString(): string
    {
        return $this->email;
    }
}