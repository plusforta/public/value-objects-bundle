<?php


namespace Plusforta\ValueObjects\Common\Document;


use Plusforta\ValueObjects\StringValueInterface;
use Webmozart\Assert\Assert;

class DocumentType implements StringValueInterface
{
    public const DOCUMENT_TYPE_APPLICATION = 'application';
    public const DOCUMENT_TYPE_CERTIFICATE = 'certificate';
    public const DOCUMENT_TYPE_RESERVATION = 'reservation';
    public const ALLOWED_DOCUMENT_TYPES = [
        self::DOCUMENT_TYPE_APPLICATION,
        self::DOCUMENT_TYPE_CERTIFICATE,
        self::DOCUMENT_TYPE_RESERVATION
    ];
    private string $documentType;

    private function __construct(string $documentType)
    {
        $this->documentType = $documentType;
    }


    public static function fromString(string $documentType): self
    {
        Assert::oneOf($documentType, self::ALLOWED_DOCUMENT_TYPES);
        return new self($documentType);
    }

    public function toString(): string
    {
        return $this->documentType;
    }

    public static function application(): self
    {
        return new self(self::DOCUMENT_TYPE_APPLICATION);
    }

    public static function reservation(): self
    {
        return new self(self::DOCUMENT_TYPE_RESERVATION);
    }

    public static function certificate(): self
    {
        return new self(self::DOCUMENT_TYPE_CERTIFICATE);
    }

}