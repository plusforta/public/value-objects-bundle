<?php


namespace Plusforta\ValueObjects\Common;


use InvalidArgumentException;
use Plusforta\ValueObjects\StringValueInterface;
use Webmozart\Assert\Assert;

class BooleanValue implements StringValueInterface
{

    public const STRING_TRUE = 'true';
    public const STRING_FALSE = 'false';
    public const ALLOWED_STRING_VALUES = [
        self::STRING_TRUE,
        self::STRING_FALSE
    ];

    private bool $value;

    private function __construct(bool $value)
    {
        $this->value = $value;
    }


    public static function fromString(string $value): self
    {
        Assert::oneOf($value, self::ALLOWED_STRING_VALUES);
        if ($value === self::STRING_TRUE) {
            return self::true();
        }

        if ($value === self::STRING_FALSE) {
            return self::false();
        }

        throw new InvalidArgumentException(sprintf('Value %s not allowed. Expected one of "true", "false"', $value));
    }

    public static function boolAsString(bool $value): string
    {
        return $value === true ? self::STRING_TRUE : self::STRING_FALSE;
    }

    public static function boolAsStringOrNull(?bool $value): ?string
    {
        if ($value === null) {
            return null;
        }

        return self::boolAsString($value);
    }

    public function toString(): string
    {
        return $this->value === true ? self::STRING_TRUE : self::STRING_FALSE;
    }

    public static function true(): self
    {
        return new self(true);
    }

    public static function false(): self
    {
        return new self(false);
    }

    public function isTrue(): bool
    {
        return $this->value === true;
    }

    public function isFalse(): bool
    {
        return $this->value === false;
    }

    public function toBool(): bool
    {
        return $this->value;
    }
}