<?php


namespace Plusforta\ValueObjects\Common\Landlord;


use Plusforta\ValueObjects\StringValueInterface;
use Webmozart\Assert\Assert;

final class LandlordGender implements StringValueInterface
{
    public const GENDER_FEMALE = 'female';
    public const GENDER_MALE = 'male';
    public const GENDER_COMPANY = 'company';

    private const ALLOWED_GENDER = [
        self::GENDER_FEMALE,
        self::GENDER_MALE,
        self::GENDER_COMPANY
    ];

    private string $gender;

    private function __construct(string $gender)
    {
        $this->gender = $gender;
    }


    public static function fromString(string $gender): self
    {
        Assert::oneOf($gender, self::ALLOWED_GENDER);
        return new self($gender);
    }

    public function toString(): string
    {
        return $this->gender;
    }

    public static function female(): self
    {
        return self::fromString(self::GENDER_FEMALE);
    }

    public static function male(): self
    {
        return self::fromString(self::GENDER_MALE);
    }

    public static function company(): self
    {
        return self::fromString(self::GENDER_COMPANY);
    }

    public function isFemale(): bool
    {
        return $this->gender === self::GENDER_FEMALE;
    }

    public function isMale(): bool
    {
        return $this->gender === self::GENDER_MALE;
    }

    public function isCompany(): bool
    {
        return $this->gender === self::GENDER_COMPANY;
    }
}