<?php


namespace Plusforta\ValueObjects\Common\Phone;


use Plusforta\ValueObjects\StringValueInterface;
use Webmozart\Assert\Assert;

final class PhoneNumber implements StringValueInterface
{
    private string $phoneNumber;

    private function __construct(string $phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }


    public static function fromString(string $phoneNumber): self
    {
        Assert::regex($phoneNumber, '/^(\+[0-9]{1,2}|0[0-9]{2,4})\s*[0-9\.\-\/\(\)]\s*[0-9\.\-\/\(\) ]+$/');
        return new self($phoneNumber);
    }

    public function toString(): string
    {
        return $this->phoneNumber;
    }
}