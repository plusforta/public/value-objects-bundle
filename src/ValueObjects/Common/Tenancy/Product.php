<?php


namespace Plusforta\ValueObjects\Common\Tenancy;


use Plusforta\ValueObjects\StringValueInterface;
use Webmozart\Assert\Assert;

final class Product implements StringValueInterface
{
    public const PRODUCT_KFX_04_2016 = 'kfx_04_2016';
    public const PRODUCT_KFDE_05_2020 = 'kfde_05_2020';
    public const PRODUCT_KFDE_06_2020 = 'kfde_06_2020';
    public const PRODUCT_KFDE_06_2022 = 'kfde_06_2022';
    public const PRODUCT_AADE_06_2020 = 'aade_06_2020';
    public const PRODUCT_HSDE_04_2021 = 'hsde_04_2021';
    public const PRODUCT_VMS_01_2022 = 'vms_01_2022';
    public const PRODUCT_GW_06_2022 = 'gw_06_2022';
    public const PRODUCT_KFDE_01_2023 = 'kfde_01_2023';

    public const PRODUCTS_KFDE = [
        self::PRODUCT_KFDE_05_2020,
        self::PRODUCT_KFDE_06_2020,
        self::PRODUCT_KFDE_06_2022,
        self::PRODUCT_KFDE_01_2023
    ];

    public const PRODUCTS_KFX = [
        self::PRODUCT_KFX_04_2016
    ];

    public const PRODUCTS_AADE = [
        self::PRODUCT_AADE_06_2020
    ];

    public const PRODUCTS_HSDE = [
        self::PRODUCT_HSDE_04_2021
    ];

    public const PRODUCTS_VMS = [
        self::PRODUCT_VMS_01_2022
    ];

    public const PRODUCTS_GW = [
        self::PRODUCT_GW_06_2022
    ];

    private const ALLOWED_PRODUCTS = [
        ...self::PRODUCTS_KFDE,
        ...self::PRODUCTS_KFX,
        ...self::PRODUCTS_AADE,
        ...self::PRODUCTS_HSDE,
        ...self::PRODUCTS_VMS,
        ...self::PRODUCTS_GW
    ];

    private string $product;

    private function __construct(string $product)
    {
        $this->product = $product;
    }

    public static function fromString(string $product): self
    {
        Assert::oneOf($product, self::ALLOWED_PRODUCTS);
        return new self($product);
    }

    public function toString(): string
    {
        return $this->product;
    }

    public function equals(?Product $product): bool
    {
        if ($product === null) {
            return false;
        }

        return $this->product === $product->toString();
    }
}