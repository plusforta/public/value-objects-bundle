<?php


namespace Plusforta\ValueObjects\Common\Tenancy;


use Plusforta\ValueObjects\StringValueInterface;
use Webmozart\Assert\Assert;

class PostalDestination implements StringValueInterface
{
    public const LEGAL_ADDRESS = 'legal_address';
    public const LANDLORD_ADDRESS = 'landlord_address';
    public const OBJECT_ADDRESS = 'object_address';
    public const DIGITAL = 'digital';

    public const ALLOWED_DESTINATIONS = [
        self::LEGAL_ADDRESS,
        self::LANDLORD_ADDRESS,
        self::OBJECT_ADDRESS,
        self::DIGITAL
    ];

    private string $postalDestination;

    private function __construct(string $postalDestination)
    {
        $this->postalDestination = $postalDestination;
    }


    public static function fromString(string $postalDestination): self
    {
        Assert::oneOf($postalDestination, self::ALLOWED_DESTINATIONS);
        return new self($postalDestination);
    }

    public function toString(): string
    {
        return $this->postalDestination;
    }

    public function isLegalAddress(): bool
    {
        return $this->postalDestination === self::LEGAL_ADDRESS;
    }

    public function isLandlorAddress(): bool
    {
        return $this->postalDestination === self::LANDLORD_ADDRESS;
    }

    public function isObjectAddress(): bool
    {
        return $this->postalDestination === self::OBJECT_ADDRESS;
    }

    public function isDigital(): bool
    {
        return $this->postalDestination === self::DIGITAL;
    }
}