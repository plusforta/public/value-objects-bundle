<?php


namespace Plusforta\ValueObjects\Common\Tenancy;


use Plusforta\ValueObjects\StringValueInterface;
use Webmozart\Assert\Assert;

final class State implements StringValueInterface
{
    public const STATE_CREATED = 'created';
    public const STATE_SUBMITTED = 'submitted';
    public const STATE_ACCEPTED = 'accepted';
    public const STATE_DECLINED = 'declined';
    public const STATE_PENDING = 'pending';

    private const ALLOWED_STATES = [
        self::STATE_CREATED,
        self::STATE_SUBMITTED,
        self::STATE_ACCEPTED,
        self::STATE_DECLINED,
        self::STATE_PENDING,
    ];

    private string $state;

    private function __construct(string $state)
    {
        $this->state = $state;
    }


    public static function fromString(string $state): self
    {
        Assert::oneOf($state, self::ALLOWED_STATES);
        return new self($state);
    }

    public function toString(): string
    {
        return $this->state;
    }

    public static function created(): self
    {
        return self::fromString(self::STATE_CREATED);
    }

    public static function submitted(): self
    {
        return self::fromString(self::STATE_SUBMITTED);
    }

    public static function accepted(): self
    {
        return self::fromString(self::STATE_ACCEPTED);
    }

    public static function declined(): self
    {
        return self::fromString(self::STATE_DECLINED);
    }

    public static function pending(): self
    {
        return self::fromString(self::STATE_PENDING);
    }

    public function isCreated(): bool
    {
        return $this->state === self::STATE_CREATED;
    }

    public function isSubmitted(): bool
    {
        return $this->state === self::STATE_SUBMITTED;
    }

    public function isAccepted(): bool
    {
        return $this->state === self::STATE_ACCEPTED;
    }

    public function isDeclined(): bool
    {
        return $this->state === self::STATE_DECLINED;
    }

    public function isPending(): bool
    {
        return $this->state === self::STATE_PENDING;
    }


}