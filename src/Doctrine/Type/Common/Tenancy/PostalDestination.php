<?php


namespace Plusforta\Doctrine\Type\Common\Tenancy;


use Plusforta\Doctrine\Type\StringValue;

class PostalDestination extends StringValue
{

    protected const TYPE_NAME = 'postal_destination';

    protected function getClassName(): string
    {
        return \Plusforta\ValueObjects\Common\Tenancy\PostalDestination::class;
    }
}