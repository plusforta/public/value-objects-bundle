<?php


namespace Plusforta\Doctrine\Type\Common\Tenancy;


use Plusforta\Doctrine\Type\StringValue;

class Product extends StringValue
{

    protected const TYPE_NAME = 'tenancy_product';

    protected function getClassName(): string
    {
        return \Plusforta\ValueObjects\Common\Tenancy\Product::class;
    }
}