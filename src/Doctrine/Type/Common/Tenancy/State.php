<?php


namespace Plusforta\Doctrine\Type\Common\Tenancy;


use Plusforta\Doctrine\Type\StringValue;

class State extends StringValue
{

    protected const TYPE_NAME = 'tenancy_state';

    protected function getClassName(): string
    {
        return \Plusforta\ValueObjects\Common\Tenancy\State::class;
    }
}