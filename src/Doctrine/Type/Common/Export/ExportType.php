<?php


namespace Plusforta\Doctrine\Type\Common\Export;



use Plusforta\Doctrine\Type\StringValue;

class ExportType extends StringValue
{
    protected const TYPE_NAME = 'export_type';

    protected function getClassName(): string
    {
        return \Plusforta\ValueObjects\Common\Export\ExportType::class;
    }
}