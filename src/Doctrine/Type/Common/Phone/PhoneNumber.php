<?php


namespace Plusforta\Doctrine\Type\Common\Phone;


use Plusforta\Doctrine\Type\StringValue;

class PhoneNumber extends StringValue
{
    protected const TYPE_NAME = 'phone_number';

    protected function getClassName(): string
    {
        return \Plusforta\ValueObjects\Common\Phone\PhoneNumber::class;
    }
}