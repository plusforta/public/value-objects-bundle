<?php


namespace Plusforta\Doctrine\Type\Common\Address;


use Plusforta\Doctrine\Type\StringValue;

final class Zip extends StringValue
{

    protected const TYPE_NAME = 'zip';

    protected function getClassName(): string
    {
        return \Plusforta\ValueObjects\Common\Address\Zip::class;
    }
}