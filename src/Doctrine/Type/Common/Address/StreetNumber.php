<?php


namespace Plusforta\Doctrine\Type\Common\Address;


use Plusforta\Doctrine\Type\StringValue;

final class StreetNumber extends StringValue
{

    protected const TYPE_NAME = 'street_number';

    protected function getClassName(): string
    {
        return \Plusforta\ValueObjects\Common\Address\StreetNumber::class;
    }

}