<?php


namespace Plusforta\Doctrine\Type\Common\Email;


use Plusforta\Doctrine\Type\StringValue;

final class EmailState extends StringValue
{
    protected const TYPE_NAME = 'email_state';

    protected function getClassName(): string
    {
        return \Plusforta\ValueObjects\Common\Email\EmailState::class;
    }
}