<?php


namespace Plusforta\Doctrine\Type\Common\Email;


use Plusforta\Doctrine\Type\StringValue;

final class EmailProvider extends StringValue
{
    protected const TYPE_NAME = 'email_provider';

    protected function getClassName(): string
    {
        return \Plusforta\ValueObjects\Common\Email\EmailProvider::class;
    }
}