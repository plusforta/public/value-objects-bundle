<?php


namespace Plusforta\Doctrine\Type\Common\Email;


use Plusforta\Doctrine\Type\StringValue;

final class Email extends StringValue
{
    protected const TYPE_NAME = 'email';

    protected function getClassName(): string
    {
        return \Plusforta\ValueObjects\Common\Email\Email::class;
    }
}