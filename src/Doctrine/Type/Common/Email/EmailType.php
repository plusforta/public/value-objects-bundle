<?php


namespace Plusforta\Doctrine\Type\Common\Email;


use Plusforta\Doctrine\Type\StringValue;

class EmailType extends StringValue
{
    protected const TYPE_NAME = 'payment_type';

    protected function getClassName(): string
    {
        return \Plusforta\ValueObjects\Common\Email\EmailType::class;
    }
}