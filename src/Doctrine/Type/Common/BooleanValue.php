<?php


namespace Plusforta\Doctrine\Type\Common;


use Plusforta\Doctrine\Type\StringValue;

class BooleanValue extends StringValue
{

    protected const TYPE_NAME = 'boolean_value';

    protected function getClassName(): string
    {
        return \Plusforta\ValueObjects\Common\BooleanValue::class;
    }
}