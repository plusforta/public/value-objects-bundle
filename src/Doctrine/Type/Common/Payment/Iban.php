<?php


namespace Plusforta\Doctrine\Type\Common\Payment;


use Plusforta\Doctrine\Type\StringValue;

class Iban extends StringValue
{
    protected const TYPE_NAME = 'payment_iban';

    protected function getClassName(): string
    {
        return \Plusforta\ValueObjects\Common\Payment\Iban::class;
    }
}