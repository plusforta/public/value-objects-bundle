<?php


namespace Plusforta\Doctrine\Type\Common\Payment;


use Plusforta\Doctrine\Type\StringValue;

class PaymentType extends StringValue
{
    protected const TYPE_NAME = 'payment_type';

    protected function getClassName(): string
    {
        return \Plusforta\ValueObjects\Common\Payment\PaymentType::class;
    }
}