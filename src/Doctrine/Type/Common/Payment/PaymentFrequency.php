<?php


namespace Plusforta\Doctrine\Type\Common\Payment;


use Plusforta\Doctrine\Type\StringValue;

class PaymentFrequency extends StringValue
{
    protected const TYPE_NAME = 'payment_frequency';

    protected function getClassName(): string
    {
        return \Plusforta\ValueObjects\Common\Payment\PaymentFrequency::class;
    }
}