<?php


namespace Plusforta\Doctrine\Type\Common\Tenant;


use Plusforta\Doctrine\Type\StringValue;

class Gender extends StringValue
{

    protected const TYPE_NAME = 'gender';

    protected function getClassName(): string
    {
        return \Plusforta\ValueObjects\Common\Tenant\Gender::class;
    }
}