<?php


namespace Plusforta\Doctrine\Type\Common\Document;



use Plusforta\Doctrine\Type\StringValue;

class DocumentType extends StringValue
{
    protected const TYPE_NAME = 'document_type';

    protected function getClassName(): string
    {
        return \Plusforta\ValueObjects\Common\Document\DocumentType::class;
    }
}