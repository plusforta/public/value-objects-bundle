<?php


namespace Plusforta\Doctrine\Type\Common\Landlord;



use Plusforta\Doctrine\Type\StringValue;

class LandlordGender extends StringValue
{
    protected const TYPE_NAME = 'landlord_gender';

    protected function getClassName(): string
    {
        return \Plusforta\ValueObjects\Common\Landlord\LandlordGender::class;
    }
}