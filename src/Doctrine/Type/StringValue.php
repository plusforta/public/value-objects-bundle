<?php


namespace Plusforta\Doctrine\Type;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use Plusforta\ValueObjects\StringValueInterface;

abstract class StringValue extends Type
{
    protected const TYPE_NAME = '';

    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $platform->getClobTypeDeclarationSQL(['length' => 255]);
    }

    /**
     * @param $value
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform
     * @return mixed|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        return $value->toString();
    }

    /**
     * @param $value
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform
     * @return mixed|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }


        /** @var StringValueInterface $valueClass */
        $valueClass = $this->getClassName();
        return $valueClass::fromString($value);
    }


    public function getName(): string
    {
        return self::TYPE_NAME;
    }

    abstract protected function getClassName(): string;
}
