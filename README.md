# Value Objects Bundle

The library offers value objects for various values. For each value object there is also a
Doctrine Type, which can be integrated into a Symfony project, allowing the Value Objects to be stored
can be stored directly in the DB.

# DEPRECATED

This is only here because someone used it everywhere.

If you want to make an enum in your projects, just make an enum.

There is no point in defining it here. Odds are you're never going to use it
in another project.

It makes NO sense to define custom database types.  This was just a bad idea that
got to far before I noticed what was going on.  Mea culpa.

It is unnecessarily complex and makes the code harder to read and understand.

If I catch you expanding this, I will be very disappointed in you.

## Validations

Why someone thought it was a good idea to run validations on data here, where it can
never be corrected by the user is beyond me.

If you want to validate data, do it in the form or the controller where the user can take
some corrective action.   Here it's too damn late.

Feel free to remove whatever validations bother you.    Better yet, stop using these
objects.

## Current Usage

| Project        | Version Used |
|----------------|:------------:|
| reports        |  dev-master  |
| export-service |  dev-master  |
| api-kfde       |  dev-master  |
| pdf-service    |  dev-master  |
| email-service  |  dev-master  |

At the time of this writing, dev-master the same as version 1.0.0

## Installation

```shell
composer require plusforta/value-objects-bundle:dev-master
```

## Usage

All value objects in the library implement the `\Plusforta\ValueObjects\StringValueInterface`.
Each object implements a `fromSting(string $value)` to create the value object
and a `toString()` method to convert the value object back into a string.

### Definition of a doctrine type

To use the Value Objects as property types in entities, the Value Object must be defined as a
Doctrine Type must be defined:

```yaml
# config/packages/doctrine.yaml
doctrine:
    dbal:
        types:
            doctrine_type_email: Plusforta\Doctrine\Type\Common\Email\Email
 ```

The value object can now be used as a property type in an entity. The key defined in the
*config/packages/doctrine.yaml* must be used as DB Type

```php
    /**
     * @ORM\Column(type="doctrine_type_email", length=255, nullable=true)
     */
    private ?Plusforta\ValueObejcts\Common\Email\Email $email = null;
```